import cv2
import numpy as np
import skimage  # pip install -U scikit-image
from skimage import filters
from matplotlib import pyplot as plt

PRE_PROCESS_IMG = False

# image = cv2.imread('SampleImages/text_images/text_deteriorated.png')
# cv2.imshow('Before Niblack Binarization', image)
names = [
    'text_deteriorated.png',
    'catacomb_Z.png',
    'newspaper.png',
    'press.png',
    'swedish_doc.png',
    'text_1.png',
    'text_deteriorated2.png',
    'typewritten_note.png',
    'Verfg1944_1.png',
    'Verfg1944_small.png',
]
images = [
    cv2.imread('SampleImages/text_images/text_deteriorated.png', 0),
    cv2.imread('SampleImages/text_images/catacomb_Z.png', 0),
    cv2.imread('SampleImages/text_images/newspaper.png', 0),
    cv2.imread('SampleImages/text_images/press.png', 0),
    cv2.imread('SampleImages/text_images/swedish_doc.png', 0),
    cv2.imread('SampleImages/text_images/text_1.png', 0),
    cv2.imread('SampleImages/text_images/text_deteriorated2.png', 0),
    cv2.imread('SampleImages/text_images/typewritten_note.png', 0),
    cv2.imread('SampleImages/text_images/Verfg1944_1.png', 0),
    cv2.imread('SampleImages/text_images/Verfg1944_small.png', 0),
]

# convert to greyscale
# grey_img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# preprocess image if wanted
# if PRE_PROCESS_IMG:
#     grey_img = cv2.bilateralFilter(grey_img, 9, 75, 75)

# plot original image histogram
# plt.subplot(1, 2, 1)
# counts, bins = np.histogram(grey_img.flatten(), 255, (0, 255))
# plt.hist(bins[:-1], bins, weights=counts)
# plt.title("Original Histogram")

# Apply Niblack Binarization
# threshold is calculated for every pixel in the image using: T = m(x,y) - k * s(x,y)
# thresh_niblack = filters.threshold_niblack(
#     grey_img, window_size=25, k=0.8)
# binary_niblack = grey_img > thresh_niblack
# binary_niblack = skimage.img_as_ubyte(binary_niblack)
# cv2.imshow('After Niblack Binarization', binary_niblack)

# multiple Niblack Binarization
# fgbg = cv2.createBackgroundSubtractorMOG2()
for i, img in enumerate(images, start=0):

    thresh_niblack = filters.threshold_niblack(
        img, window_size=25, k=0.8)
    binary_niblack = img > thresh_niblack
    binary_niblack = skimage.img_as_ubyte(binary_niblack)
    # if PRE_PROCESS_IMG:
    #     binary_niblack = cv2.bilateralFilter(binary_niblack, 9, 75, 75)

    # sauvola
    # thresh_sauvola = filters.threshold_sauvola(img, window_size=25)
    # binary_sauvola = img > thresh_sauvola
    # binary_sauvola = skimage.img_as_ubyte(binary_sauvola)

    cv2.imshow(names[i], binary_niblack)
    # cv2.imshow(names[i], binary_sauvola)

# plot Niblack binarized histogram
# plt.subplot(1, 2, 2)
# counts, bins = np.histogram(binary_niblack.flatten(), 255, (0, 255))
# plt.hist(bins[:-1], bins, weights=counts)
# plt.title("Niblack Binarization Histogram")

# plt.show()
cv2.waitKey()
