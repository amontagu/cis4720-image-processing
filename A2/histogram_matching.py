from skimage import exposure
import numpy as np
import matplotlib.pyplot as plt
import cv2
from sewar.full_ref import uqi

image_src = cv2.imread('test_images/hawkesbayNZ_contrast.png', 0)
image_ref = cv2.imread('test_images/eg1_grndtruth.jpg', 0)

matched = exposure.match_histograms(image_src, image_ref, multichannel=False)
matched_image = np.array(matched, dtype=np.uint8)

# Add UQI to figure
plt.figtext(0.2, 0.01, f'Universal Quality Image Index: {uqi(image_src, matched_image)}')

# plot first histogram
plt.subplot(1, 3, 1)
counts, bins = np.histogram(image_src, 255, (0, 255))
plt.hist(bins[:-1], bins, weights=counts)
plt.title("Original Histogram")

# plot second histogram
plt.subplot(1, 3, 2)
counts, bins = np.histogram(image_ref, 255, (0, 255))
plt.hist(bins[:-1], bins, weights=counts)
plt.title("Reference Histogram")

# plot second histogram
plt.subplot(1, 3, 3)
counts, bins = np.histogram(matched_image, 255, (0, 255))
plt.hist(bins[:-1], bins, weights=counts)
plt.title("Matched Histogram")

plt.subplots_adjust(wspace=0.5)
plt.show()

# show the output images
cv2.imshow("Reference", image_ref)
cv2.imshow('Original', image_src)
cv2.imshow('Matched', matched_image)
cv2.waitKey(0)