import cv2
import numpy as np
from matplotlib import pyplot as plt
from sewar.full_ref import uqi

image = cv2.imread('test_images/hawkesbayNZ_contrast.png', 0)
mean = int(np.mean(image))

# Create empty arrays for sub images
xl = np.zeros((image.shape[0],image.shape[1]))
xu = np.zeros((image.shape[0],image.shape[1]))

# Setting up sub images of range (0, mean) and (mean, 255)
for i in range(image.shape[0]):
    for j in range(image.shape[1]):
        if image[i,j] <= mean:
            xl[i,j] = image[i,j]
            xu[i,j] = -1
        else:
            xu[i,j] = image[i,j]
            xl[i,j] = -1

# Plot histogram of original image
plt.subplot(2, 2, 1)
hist1, bins = np.histogram(image.flatten(), 256, [0, 256])
plt.hist(bins[:-1], bins, weights=hist1)
plt.title("Original Image")

# Plot histogram of lower subimage
plt.subplot(2, 2, 2)
xl_hist, bins = np.histogram(xl, 256, [0, 256])
plt.hist(bins[:-1], bins, weights=xl_hist)
plt.title("SubImage1")

# Plot histogram of upper subimage
plt.subplot(2, 2, 3)
xu_hist, bins = np.histogram(xu, 256, [0, 256])
plt.hist(bins[:-1], bins, weights=xu_hist)
plt.title("SubImage2")

# Set the lower max and mins
xl_min = 0
xl_max = mean

# Set the upper max and mins
xu_min = mean + 1
xu_max = 255

# Normalize the lower histogram for the subimage
p_xl = np.sum(xl_hist)
xl_hist = xl_hist/p_xl

# Normalize the upper histogram for subimage
p_xu = np.sum(xu_hist)
xu_hist = xu_hist/p_xu

# Calculate the cummulative sums for each subimage
xl_cumsum = np.cumsum(xl_hist)
xu_cumsum = np.cumsum(xu_hist)

# Set array for output
final_image = np.zeros((image.shape[0], image.shape[1]), dtype = 'uint8')

# Bound subimages together on the mean
for i in range(image.shape[0]):
    for j in range(image.shape[1]):
        if (image[i,j] <= mean):
            final_image[i,j] = xl_min + (xl_max - xl_min) * xl_cumsum[image[i, j]]
        else:
            final_image[i,j] = xu_min + (xu_max - xu_min) * xu_cumsum[image[i, j]]

# Plot final histogram
plt.subplot(2, 2, 4)
plt.title("New Histogram")
counts, bins = np.histogram(final_image.flatten(), 256, [0, 256])
plt.hist(bins[:-1], bins, weights=counts)

# Add UQI to figure
plt.figtext(0.2, 0.01, f'Universal Quality Image Index: {uqi(image, final_image)}')

cv2.imshow('Original', image)
cv2.imshow('Adjusted', final_image)

plt.subplots_adjust(wspace=0.5, hspace=0.5)
plt.show()
cv2.waitKey()
