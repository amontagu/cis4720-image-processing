import cv2
import numpy as np
import skimage  # pip install -U scikit-image
from skimage import filters
from matplotlib import pyplot as plt

PRE_PROCESS_IMG = False

image = cv2.imread('SampleImages/text_images/text_deteriorated.png')
cv2.imshow('Before Otsu Binarization', image)

# convert to greyscale
grey_img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# preprocess image if wanted
if PRE_PROCESS_IMG:
    grey_img = cv2.bilateralFilter(grey_img, 9, 75, 75)

# plot original image histogram
plt.subplot(1, 2, 1)
counts, bins = np.histogram(grey_img.flatten(), 255, (0, 255))
plt.hist(bins[:-1], bins, weights=counts)
plt.title("Original Histogram")

# Apply Otsu Binarization
ret, otsu_binarized_img = cv2.threshold(
    grey_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
cv2.imshow('After Otsu Binarization', otsu_binarized_img)

# plot Otsu binarized histogram
plt.subplot(1, 2, 2)
counts, bins = np.histogram(otsu_binarized_img.flatten(), 255, (0, 255))
plt.hist(bins[:-1], bins, weights=counts)
plt.title("Otsu Binarization Histogram")

plt.show()
cv2.waitKey()
