import cv2
import numpy as np
from matplotlib import pyplot as plt
from sewar.full_ref import uqi

image = cv2.imread('test_images/hawkesbayNZ_contrast.png', 0)
cv2.imshow('Before CLAHE', image)

# Apply CLAHE
clahe = cv2.createCLAHE(clipLimit=2, tileGridSize=(8,8))
image_clahe = clahe.apply(image)
cv2.imshow('After CLAHE', image_clahe)

# Add UQI to figure
plt.figtext(0.2, 0.01, f'Universal Quality Image Index: {uqi(image, image_clahe)}')

# plot first histogram
plt.subplot(1, 2, 1)
counts, bins = np.histogram(image.flatten(), 255, (0, 255))
plt.hist(bins[:-1], bins, weights=counts)
plt.title("Original Histogram")

# plot second histogram
plt.subplot(1, 2, 2)
counts, bins = np.histogram(image_clahe.flatten(), 255, (0, 255))
plt.hist(bins[:-1], bins, weights=counts)
plt.title("CLAHE Histogram")

plt.show()
cv2.waitKey()