import cv2
import numpy as np
from matplotlib import pyplot as plt

# Test image
img = cv2.imread('SampleImages/text_images/text_deteriorated.png', 0)

# Pre processing
# img = cv2.bilateralFilter(img, 9, 75, 75)

# Apply thresholding
adaptive_gaussian = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                          cv2.THRESH_BINARY, 11, 2)

cv2.imshow('Original', img)
cv2.imshow('Adaptive Gaussian', adaptive_gaussian)
cv2.waitKey()
