import cv2
import numpy as np
from matplotlib import pyplot as plt
from sewar.full_ref import uqi

image = cv2.imread('test_images/contrast2.jpg', 0)
cv2.imshow('Before Contrast Stretched', image)

# Create an empty array to store the final output
image_cs = np.zeros((image.shape[0],image.shape[1]), dtype = 'uint8')

# Apply Min-Max Contrasting
min = np.min(image)
max = np.max(image)

for i in range(image.shape[0]):
    for j in range(image.shape[1]):
        image_cs[i,j] = 255*(image[i,j]-min)/(max-min)

cv2.imshow('After Contrast Stretched', image_cs)

# Add UQI to figure
plt.figtext(0.2, 0.01, f'Universal Quality Image Index: {uqi(image, image_cs)}')

plt.subplot(1, 2, 1)
counts, bins = np.histogram(image.flatten(), 255, (0, 255))
plt.hist(bins[:-1], bins, weights=counts)
plt.title("Original Histogram")

plt.subplot(1, 2, 2)
counts, bins = np.histogram(image_cs.flatten(), 255, (0, 255))
plt.hist(bins[:-1], bins, weights=counts)
plt.title("Contrast Stretched Histogram")
plt.show()

cv2.waitKey()