# CIS*4720 Image Processing

### Alex Montague - 0959687
### Samir Haq - 0968761

- This repository is for all of the code used in our image processing assignments. 
- Simply install the python dependencies and run the programs with `python <FILE_NAME>`
- Make sure you are using Python3

### A2
- All Assignment Two files are located in the A2 directory
- The report is cis*4720_amontagu_samir_a2.pdf
- All test images are located in the test_images directory

### A3
- All Assignment Three files are located in the A3 directory
- The report is cis4720_amontagu_samir_a3.pdf
- All test images are located in the SampleImages directory
- To run programs run using `python <programname>`
- To test specific images change filepath in `cv2.imread(<filepath>)` to the wanted image
- For adaptive binarization and adaptive gaussian algorithms, uncomment the line under the pre processing comment to run it with bilateral filter.